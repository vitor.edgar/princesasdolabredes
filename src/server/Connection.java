package server;

import server.components.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Connection {

    private static ArrayList<Player> players = new ArrayList<>();
    private static int maxNumberOfPlayers = 2;
    private static Game game;
    private static ServerSocket server;

    public static void main(String args[]){

        awaitClients();

    }

    public static void awaitClients() {

        try {
            server = new ServerSocket(3000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (players.size() < maxNumberOfPlayers){

            System.out.println("Aguardando Jogadores: ");

            Socket socket = null;
            try {
                socket = server.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Client " + socket.getInetAddress().getHostAddress() + " Conectado na Porta " + server.getLocalPort());

            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            String nickname = null;
            try {
                nickname = (String) ois.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            System.out.println("Nickname " + nickname);

            Player player = new Player(socket, nickname, players.size());
            players.add(player);

            ObjectOutputStream oos = null;
            try {
                oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject(player.getId());
            } catch (IOException e) {
                e.printStackTrace();
            }

            player.oos = oos;
            player.ois = ois;

        }

        startGame();

    }

    public static void startGame(){

        game = new Game(players.size());

        updateClientsBoards();
        updateClientsPlayers();

        while(game.isPlaying()){

            Player player = null;
            for (Player p : players){
                if(game.getPlayersTurn() == p.getId()) {
                    player = p;
                    break;
                }
            }

            notifyPlayerTurn(player);
            awaitPlayerMove(player);

        }

        players = new ArrayList<>();

        awaitClients();

    }

    public static void awaitPlayerMove(Player player){

        ObjectInputStream ois = null;
        int diceNumber;
        try {
            ois = player.ois;
            diceNumber = (int) ois.readObject();
            System.out.println("Jogador " + player.getNickname() + " moveu-se " + diceNumber + " casas");
            game.move(player, diceNumber);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void updateClientsBoards(){

        for (Player p : players){
            ObjectOutputStream oos;
            try {
                oos = p.oos;
                oos.writeObject(game.getBoard().getFields());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void updateClientsPlayers(){

        ArrayList<ArrayList> playersAux = new ArrayList<>();

        for (Player p : players){
            ArrayList<String> aux = new ArrayList<>();
            aux.add(Integer.toString(p.getId()));
            aux.add(p.getNickname());
            playersAux.add(aux);
        }

        for (Player p : players){
            ObjectOutputStream oos;
            try {
                oos = p.oos;
                oos.writeObject(playersAux);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void updateClientsPlayersPositions(){

        notifyPlayersUpdatePositions();
        ArrayList<Integer> playersAux = new ArrayList<>();

        for (Player p : players){
            playersAux.add(p.getId());
            playersAux.add(p.getFieldIndex());
        }

        for (Player p : players){
            ObjectOutputStream oos;
            try {
                oos = p.oos;
                oos.writeObject(playersAux);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void notifyPlayerTurn(Player player){

        ObjectOutputStream oos;
        try {
            oos = player.oos;
            oos.writeObject("Your Turn");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void notifyPlayersUpdatePositions(){

        for(Player p : players){
            ObjectOutputStream oos;
            try {
                oos = p.oos;
                oos.writeObject("Update Position");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void finishGame(){

        for(Player p : players){
            ObjectOutputStream oos;
            try {
                oos = p.oos;
                oos.writeObject("Finish Game");
                p.ois.close();
                p.oos.close();
                p.getSocket().close();
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Jogo Encerrado! Servidor pronto para uma nova partida");

    }

    public static void notifyPlayersFieldType(String msg){

        for(Player p : players){
            ObjectOutputStream oos;
            try {
                oos = p.oos;
                oos.writeObject(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
