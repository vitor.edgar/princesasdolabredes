package server.components;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Player {

    private Socket socket;
    private String nickname;
    private int id;
    private int fieldIndex;
    public ObjectOutputStream oos;
    public ObjectInputStream ois;

    public Player(Socket socket, String nickname, int id) {
        this.socket = socket;
        this.nickname = nickname;
        this.id = id;
        this.fieldIndex = 0;
    }

    public Socket getSocket() {
        return socket;
    }

    public String getNickname() {
        return nickname;
    }

    public int getId() {
        return id;
    }

    public int getFieldIndex() {
        return fieldIndex;
    }

    public void setFieldIndex(int fieldIndex) {
        this.fieldIndex = fieldIndex;
    }
}
