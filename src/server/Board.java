package server;

import server.components.Field;
import server.components.FieldType;

import java.io.Serializable;
import java.util.ArrayList;

public class Board implements Serializable {

    private ArrayList<Field> fields = new ArrayList<>();

    public Board(int boardSize) {
        for (int i = 0; i<=(boardSize+1); i++){
            Field f;
            if(i == 0){
                f = new Field(FieldType.START);
            }
            else if(i == (boardSize+1)){
                f = new Field(FieldType.FINISH);
            }
            else if((i%3) == 0){
                f = new Field(FieldType.BACKWARD);
                if ((i%2) == 0){
                    f.setValue(1);
                }
                else f.setValue(2);
            }
            else if((i%4) == 0){
                f = new Field(FieldType.FOWARD);
                f.setValue(3);
            }
            else if((i%2) == 0){
                f = new Field(FieldType.AGAIN);
            }
            else {
                f = new Field(FieldType.JOKE);
            }
            fields.add(f);
        }
    }

    public ArrayList<Field> getFields() {
        return fields;
    }
}
