package server;

import server.components.FieldType;
import server.components.Player;

public class Game {

    private boolean isPlaying;
    private int playersTurn;
    private int numberOfPlayers;
    private Board board;

    public Game(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        this.board = new Board(30);
        this.isPlaying = true;
        startGame();
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void startGame(){
        playersTurn = 0;
    }

    public void move(Player player, int diceNumber) {

        String msg = null;
        player.setFieldIndex((player.getFieldIndex()+diceNumber));

        if (player.getFieldIndex() >= (board.getFields().size()-1)){
            player.setFieldIndex(board.getFields().size()-1);
        }

        if (board.getFields().get(player.getFieldIndex()).getType() != FieldType.AGAIN){
            nextPlayer();
        }

        Connection.updateClientsPlayersPositions();

        if (board.getFields().get(player.getFieldIndex()).getType() == FieldType.FINISH){
            this.isPlaying = false;
            Connection.notifyPlayersFieldType("");
            Connection.finishGame();
            return;
        }
        else if(board.getFields().get(player.getFieldIndex()).getType() == FieldType.AGAIN){
            msg = "UHU!! O jogador " + player.getNickname() + " joga novamente";
        }
        else if(board.getFields().get(player.getFieldIndex()).getType() == FieldType.FOWARD){
            msg = "Parabéns! O jogador " + player.getNickname() + " irá andar mais " + board.getFields().get(player.getFieldIndex()).getValue() + " casas";
            player.setFieldIndex((player.getFieldIndex()+board.getFields().get(player.getFieldIndex()).getValue()));

            if (board.getFields().get(player.getFieldIndex()).getType() == FieldType.FINISH){
                this.isPlaying = false;
                Connection.notifyPlayersFieldType("");
                Connection.finishGame();
                return;
            }

        }
        else if(board.getFields().get(player.getFieldIndex()).getType() == FieldType.BACKWARD){
            msg = "Bish! O jogador " + player.getNickname() + " irá retornar " + board.getFields().get(player.getFieldIndex()).getValue() + " casas";
            player.setFieldIndex((player.getFieldIndex()-board.getFields().get(player.getFieldIndex()).getValue()));
        }
        else msg = "É! " + player.getNickname() + " Nada acontece. Conte uma piada!!";

        Connection.notifyPlayersFieldType(msg);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        Connection.updateClientsPlayersPositions();
        Connection.notifyPlayersFieldType("");

    }

    public void nextPlayer(){
        playersTurn++;
        if(playersTurn == numberOfPlayers) playersTurn = 0;
    }

    public Board getBoard() {
        return board;
    }

    public int getPlayersTurn() {
        return playersTurn;
    }
}
